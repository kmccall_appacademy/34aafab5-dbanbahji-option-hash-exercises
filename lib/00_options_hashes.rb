# Options Hashes
#
# Write a method `transmogrify` that takes a `String`. This method should
# take optional parameters `:times`, `:upcase`, and `:reverse`. Hard-code
# reasonable defaults in a `defaults` hash defined in the `transmogrify`
# method. Use `Hash#merge` to combine the defaults with any optional
# parameters passed by the user. Do not modify the incoming options
# hash. For example:
#
# ```ruby
# transmogrify("Hello")                                    #=> "Hello"
# transmogrify("Hello", :times => 3)                       #=> "HelloHelloHello"
# transmogrify("Hello", :upcase => true)                   #=> "HELLO"
# transmogrify("Hello", :upcase => true, :reverse => true) #=> "OLLEH"
#
# options = {}
# transmogrify("hello", options)
# # options shouldn't change.
# ```
def transmogrify(string, optional_parameter = {})
  defaults = {
    times: 1,
    upcase: false,
    reverse: false
  }
  new_str = ""
  optional_parameter = defaults.merge(optional_parameter)

  if optional_parameter[:upcase]
    string = string.upcase
  end

  if optional_parameter[:reverse]
    string = string.reverse
  end

  if optional_parameter[:times]
      optional_parameter[:times].times do
        new_str += string
      end
  end
new_str
end
